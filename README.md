# Переклад гри Star Citizen Українською 
[![valdeus/sc_localization_ua](https://img.shields.io/gitlab/v/release/valdeus%2Fsc_localization_ua)](https://gitlab.com/valdeus/sc_localization_ua/-/releases)
[![Crowdin](https://badges.crowdin.net/star-citizen-localization-ua/localized.svg)](https://shorturl.at/dopMW)
[![Website](https://img.shields.io/website?url=https%3A%2F%2Fusf.42web.io%2F&down_message=3.24&style=flat&label=SC&labelColor=blue&color=blue)](https://robertsspaceindustries.com/spectrum/community/SC/forum/50172/thread/localization-ukrainian/404478)
[![License](https://img.shields.io/static/v1?label=license&message=CC-BY-NC-SA-4.0&color=green)](https://gitlab.com/valdeus/sc_localization_ua/-/blob/main/LICENSE)
[![Website](https://img.shields.io/website?url=https%3A%2F%2Fusf.42web.io%2F&down_message=SITE&style=flat&label=USF&labelColor=blue&color=yellow)](https://usf.42web.io/)

[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[![bug-report](https://img.shields.io/badge/bug-report-blue?style=for-the-badge)](https://shorturl.at/dkwN7)



[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg

Дякуємо всім, хто взяв участь у перекладі!:

[![Static Badge](https://img.shields.io/badge/UKRAINIAN%20SPACE%20FLEET-%20?style=plastic&label=CORP&labelColor=%23196ea7&color=%23fbe21b)](https://robertsspaceindustries.com/orgs/UKR)
[![Static Badge](https://img.shields.io/badge/UACOM-%20?style=plastic&label=CORP&labelColor=%23196ea7&color=%23fbe21b)](https://robertsspaceindustries.com/orgs/UACOM)
[![Static Badge](https://img.shields.io/badge/FREE%20SPACE%20PROJECT-%20?style=plastic&label=CORP&labelColor=%23196ea7&color=%23fbe21b)](https://robertsspaceindustries.com/orgs/FSPROJECT)
[![Static Badge](https://img.shields.io/badge/CODEGAMING-%20?style=plastic&label=CORP&labelColor=%23196ea7&color=%23fbe21b)](https://robertsspaceindustries.com/orgs/CODEGAMING)

[![Static Badge](https://img.shields.io/badge/SQUAD%20UKRAINE-%20?style=plastic&label=CORP&labelColor=%23196ea7&color=%23fbe21b)](https://robertsspaceindustries.com/orgs/UKRAINESQ)
[![Static Badge](https://img.shields.io/badge/TRIDENT-%20?style=plastic&label=CORP&labelColor=%23196ea7&color=%23fbe21b)](https://robertsspaceindustries.com/orgs/UKRAINE)
[![Static Badge](https://img.shields.io/badge/UKRA%D0%86NIAN%20DEMOCRATIC%20CORP-%20?style=plastic&label=CORP&labelColor=%23196ea7&color=%23fbe21b)](https://robertsspaceindustries.com/orgs/UKRAINIANS)

Підтримка локалізації гри зроблена учасниками [UKRAINIAN SPACE FLEET](https://robertsspaceindustries.com/orgs/UKR)

Проєкт на стадії полірування

Повідомити про помилку в перекладі або приєднатися до проєкту >>> [![](https://dcbadge.vercel.app/api/server/TkaN6Yv4VT?style=flat&theme=clean&compact=true)](https://discord.com/invite/TkaN6Yv4VT)

## Встановлення

### Ручний спосіб
Спосіб №1
1. Завантажте останню версію перекладу з 
2. Перенесіть теку `Data` із завантаженого архіву, до теки `[StarCitizen\LIVE]`
3. До вашого `user.cfg` додайте два рядки під іншими рядками, якщо такі є:

```plaintext
g_language = korean_(south_korea)
g_languageAudio=english
```
> 💡 **Примітка**
>
> Якщо у вас немає файлу `user.cfg`, ви можете створити його:
>
> 1. Створіть текстовий файл і назвіть його `user.txt`
> 2. Змініть розширення файлу з `.txt` на `.cfg`
> 3. Збережіть `user.cfg` в папці: `[StarCitizen\LIVE]`
> 4. Тепер ви можете відкрити та відредагувати `user.cfg` за допомогою Блокнота або інших текстових редакторів.

> ⚠️ **Важливо**
>
> Переконайтеся, що ваш файл `user.cfg` має кодування UTF-8 (не, наприклад, UTF-8 with BOM).

Спосіб №2
1. Завантажте архів перекладу з 
2. Розпакуйте архів до теки `[StarCitizen\LIVE]`

### Автоматично
через додаток - [SCLOCAPP](https://gitlab.com/valdeus/sclocapp/-/archive/1.5.4.0/sclocapp-1.5.4.0.zip)
